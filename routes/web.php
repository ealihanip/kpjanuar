<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth', 'role:admin']], function () {
    Route::namespace('admin')->group(function () {
        Route::resource('admin/guru', 'GuruController');
        Route::resource('admin/matapelajaran', 'MatapelajaranController');
        Route::resource('admin/ekstrakulikuler', 'EkstrakulikulerController');
        Route::resource('admin/mengajarmatapelajaran', 'MengajarmatapelajaranController');
        Route::resource('admin/mengajarekstrakulikuler', 'MengajarekstrakulikulerController');
        Route::resource('admin/dataabsenmatapelajaran', 'AbsenmapelController');
        Route::resource('admin/dataabsenekstrakulikuler', 'AbsenekstrakulikulerController');
        Route::resource('admin/user', 'UserController');
        Route::get('/admin', function () {
            return view('admin.index');
        });
    });
});

Route::group(['middleware' => ['auth', 'role:guru']], function () {
    Route::namespace('guru')->group(function () {

        Route::resource('guru/absenmatapelajaran', 'AbsenmapelController');
        Route::resource('guru/absenekstrakulikuler', 'AbsenekstrakulikulerController');
        Route::get('/guru', function () {
            return view('guru.index');
        });
       
    });
});





Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
