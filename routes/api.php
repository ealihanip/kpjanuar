<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('admin')->group(function () {
    Route::get('admin/guru/getdata', 'GuruController@getdata');
    Route::get('admin/matapelajaran/getdata', 'MatapelajaranController@getdata');
    Route::get('admin/ekstrakulikuler/getdata', 'EkstrakulikulerController@getdata');
    Route::get('admin/mengajarmatapelajaran/getdata', 'MengajarmatapelajaranController@getdata');
    Route::get('admin/mengajarekstrakulikuler/getdata', 'MengajarekstrakulikulerController@getdata');
    Route::get('admin/absenmatapelajaran/getdataguru', 'AbsenmatapelajaranController@getdataguru');
    Route::get('user/getdata', 'UserController@getdata');
});