<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Absenekstrakulikuler extends Model
{
    protected $table="absenekstrakulikuler";
    protected $primaryKey="id";
}
