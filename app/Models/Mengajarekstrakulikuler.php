<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Mengajarekstrakulikuler extends Model
{
    protected $table="mengajarekstrakulikuler";
    protected $primaryKey="id";

    public function guru()
    {
        return $this->belongsTo('App\Models\Guru');
    }

    public function ekstrakulikuler()
    {
        return $this->belongsTo('App\Models\Ekstrakulikuler');
    }

    public function absenekstrakulikuler()
    {
        return $this->hasMany('App\Models\Absenekstrakulikuler');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($data) { // before delete() method call this
             $data->absenekstrakulikuler()->delete();
             // do the rest of the cleanup...
        });
    }
}
