<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mengajarmatapelajaran extends Model
{
    protected $table="mengajarmatapelajaran";
    protected $primaryKey="id";

    public function guru()
    {
        return $this->belongsTo('App\Models\Guru');
    }

    public function matapelajaran()
    {
        return $this->belongsTo('App\Models\Matapelajaran');
    }

    public function absenmatapelajaran()
    {
        return $this->hasMany('App\Models\Absenmapel');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($data) { // before delete() method call this
             $data->absenmatapelajaran()->delete();
             // do the rest of the cleanup...
        });
    }
}
