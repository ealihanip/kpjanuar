<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table="guru";
    protected $primaryKey="id";

    public function mengajarmatapelajaran()
    {
        return $this->hasMany('App\Models\Mengajarmatapelajaran');
    }

    public function absenmatapelajaran()
    {
        return $this->hasManyThrough('App\Models\Absenmapel', 'App\Models\Mengajarmatapelajaran');
    }

    public function mengajarekstrakulikuler()
    {
        return $this->hasMany('App\Models\Mengajarekstrakulikuler');
    }

    public function absenekstrakulikuler()
    {
        return $this->hasManyThrough('App\Models\Absenekstrakulikuler', 'App\Models\Mengajarekstrakulikuler');
    }


    public static function boot() {
        parent::boot();

        static::deleting(function($data) { // before delete() method call this
            $data->mengajarekstrakulikuler()->delete();
            $data->mengajarmatapelajaran()->delete();
            
        });
    }


    
}
