<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matapelajaran extends Model
{
    protected $table="matapelajaran";
    protected $primaryKey="id";

    public function mengajarmatapelajaran()
    {
        return $this->hasMany('App\Models\Mengajarmatapelajaran');
    }
}
