<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth=Auth::user()->roleuser;
        

        foreach($auth as $auth){

          $role=$auth->role_id;
          
        }
        
        if($role==1){
            return redirect('admin');
        }

        if($role==2){
            return redirect('guru');
        }

       
        
    }
}
