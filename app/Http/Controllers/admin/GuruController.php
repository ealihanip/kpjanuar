<?php

namespace App\Http\Controllers\admin;
use App\User;
use App\Models\Guru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreGuru;
use App\Http\Requests\UpdateGuru;


class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.guru.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata()
    {
        $data = Guru::all();


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="guru/'.$data->id.'/edit" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a onclick="comfirmdelete('.$data->id.')" class="btn btn-xs btn-danger text-white"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    public function create()
    {
        return view('admin.guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGuru $request)
    {
        

        //simpan ke user
        $user = new User;
        $user->name=$request->nama;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->save();
        $user_id=$user->id;
        
        //memberi role user
        $user->roles()->attach(2);

        //simpan ke guru
        $guru = new Guru;
        $guru->nama = $request->nama;
        $guru->user_id = $user_id;
        $guru->save();
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function show(Guru $guru)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function edit(Guru $guru)
    {

        $user=User::find($guru->user_id);
        $data['guru']=$guru;
        $data['data']=$user;
        return view('admin.guru.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGuru $request, Guru $guru)
    {


        
        
        $guru = Guru::find($guru->id);

        $guru->nama = $request->nama;

        $guru->save();

        $user = User::find($guru->user_id);

        $user->name=$request->nama;
        

        //jika ubah password
        if($user->email!=$request->email){

            $request->validate([
                'email' => 'required|string|email|max:255|unique:users'
            ]);
            
            $user->email=$request->email;
        }

        if(!empty($request->password)){

            $request->validate([
                'password' => 'required|string|min:6|confirmed'
            ]);
            $user->password=Hash::make($request->password);
            
        }

        $user->save();
        
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Guru  $guru
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guru $guru)
    {   
        $user = User::find($guru->user_id);
        $user->delete();

        return redirect()->back();
    }
}
