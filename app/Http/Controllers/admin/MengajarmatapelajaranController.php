<?php

namespace App\Http\Controllers\admin;

use App\Models\Mengajarmatapelajaran;
use App\Models\Guru;
use App\Models\Matapelajaran;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class MengajarmatapelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.mengajarmatapelajaran.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata()
    {

        $mengajarmatapelajaran = Mengajarmatapelajaran::with(['guru'])->with(['matapelajaran'])->get();

        

        $data = $mengajarmatapelajaran;


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="mengajarmatapelajaran/'.$data->id.'/edit" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a onclick="comfirmdelete('.$data->id.')" class="btn btn-xs btn-danger text-white"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    public function create()
    {
        $guru = Guru::all();

        $data['guru']=$guru;

        $matapelajaran = Matapelajaran::all();

        $data['matapelajaran']=$matapelajaran;
        
        return view('admin.mengajarmatapelajaran.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $mengajarmatapelajaran = new Mengajarmatapelajaran;
        $mengajarmatapelajaran->guru_id = $request->guru;
        $mengajarmatapelajaran->matapelajaran_id = $request->matapelajaran;
        $mengajarmatapelajaran->save();

        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Mengajarmatapelajaran  $mengajarmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(Mengajarmatapelajaran $mengajarmatapelajaran)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Mengajarmatapelajaran  $mengajarmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Mengajarmatapelajaran $mengajarmatapelajaran)
    {
        $guru = Guru::all();

        $data['guru']=$guru;

        $matapelajaran = Matapelajaran::all();

        $data['matapelajaran']=$matapelajaran;

        $data['data']=$mengajarmatapelajaran;
        
        return view('admin.mengajarmatapelajaran.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Mengajarmatapelajaran  $mengajarmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mengajarmatapelajaran $mengajarmatapelajaran)
    {
        
        $mengajarmatapelajaran = Mengajarmatapelajaran::find($mengajarmatapelajaran->id);
        $mengajarmatapelajaran->guru_id = $request->guru;
        $mengajarmatapelajaran->matapelajaran_id = $request->matapelajaran;
        $mengajarmatapelajaran->save();
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Mengajarmatapelajaran  $mengajarmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mengajarmatapelajaran $mengajarmatapelajaran)
    {

        Mengajarmatapelajaran::destroy($mengajarmatapelajaran->id);
        
        return redirect()->back();
    }
}
