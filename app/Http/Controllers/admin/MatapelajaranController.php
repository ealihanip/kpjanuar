<?php

namespace App\Http\Controllers\admin;

use App\Models\Matapelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\Storematapelajaran;

class MatapelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.matapelajaran.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata()
    {
        $data = Matapelajaran::all();


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="matapelajaran/'.$data->id.'/edit" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a onclick="comfirmdelete('.$data->id.')" class="btn btn-xs btn-danger text-white"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    public function create()
    {
        return view('admin.matapelajaran.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMatapelajaran $request)
    {
        
        $matapelajaran = new Matapelajaran;
        $matapelajaran->nama = $request->nama;
        $matapelajaran->save();

        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Matapelajaran  $matapelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(Matapelajaran $matapelajaran)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Matapelajaran  $matapelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Matapelajaran $matapelajaran)
    {

        $data['data']=$matapelajaran;
        $data['url']=$url = route('matapelajaran.index');;
        return view('admin.matapelajaran.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Matapelajaran  $matapelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMatapelajaran $request, Matapelajaran $matapelajaran)
    {
        
        $matapelajaran = Matapelajaran::find($matapelajaran->id);

        $matapelajaran->nama = $request->nama;

        $matapelajaran->save();
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Matapelajaran  $matapelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matapelajaran $matapelajaran)
    {
        Matapelajaran::destroy($matapelajaran->id);
        return redirect()->back();
    }
}
