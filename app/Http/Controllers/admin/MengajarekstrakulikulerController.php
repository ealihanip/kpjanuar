<?php

namespace App\Http\Controllers\admin;

use App\models\Mengajarekstrakulikuler;
use App\Models\Guru;
use App\Models\Ekstrakulikuler;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class MengajarekstrakulikulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.mengajarekstrakulikuler.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata()
    {

        $mengajarekstrakulikuler = Mengajarekstrakulikuler::with(['guru'])->with(['ekstrakulikuler'])->get();

        

        $data = $mengajarekstrakulikuler;


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="mengajarekstrakulikuler/'.$data->id.'/edit" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a onclick="comfirmdelete('.$data->id.')" class="btn btn-xs btn-danger text-white"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    public function create()
    {
        $guru = Guru::all();

        $data['guru']=$guru;

        $ekstrakulikuler = Ekstrakulikuler::all();

        $data['ekstrakulikuler']=$ekstrakulikuler;
        
        return view('admin.mengajarekstrakulikuler.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $mengajarekstrakulikuler = new Mengajarekstrakulikuler;
        $mengajarekstrakulikuler->guru_id = $request->guru;
        $mengajarekstrakulikuler->ekstrakulikuler_id = $request->ekstrakulikuler;
        $mengajarekstrakulikuler->save();

        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Mengajarekstrakulikuler  $mengajarekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function show(Mengajarekstrakulikuler $mengajarekstrakulikuler)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Mengajarekstrakulikuler  $mengajarekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function edit(Mengajarekstrakulikuler $mengajarekstrakulikuler)
    {
        $guru = Guru::all();

        $data['guru']=$guru;

        $ekstrakulikuler = Ekstrakulikuler::all();

        $data['ekstrakulikuler']=$ekstrakulikuler;

        $data['data']=$mengajarekstrakulikuler;
        
        return view('admin.mengajarekstrakulikuler.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Mengajarekstrakulikuler  $mengajarekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mengajarekstrakulikuler $mengajarekstrakulikuler)
    {
        
        $mengajarekstrakulikuler = Mengajarekstrakulikuler::find($mengajarekstrakulikuler->id);
        $mengajarekstrakulikuler->guru_id = $request->guru;
        $mengajarekstrakulikuler->ekstrakulikuler_id = $request->ekstrakulikuler;
        $mengajarekstrakulikuler->save();
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Mengajarekstrakulikuler  $mengajarekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mengajarekstrakulikuler $mengajarekstrakulikuler)
    {
        Mengajarekstrakulikuler::destroy($mengajarekstrakulikuler->id);
        return redirect()->back();
    }
}
