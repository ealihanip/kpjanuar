<?php

namespace App\Http\Controllers\admin;

use App\Models\Ekstrakulikuler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\StoreEkstrakulikuler;

class EkstrakulikulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.ekstrakulikuler.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata()
    {
        $data = Ekstrakulikuler::all();


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="ekstrakulikuler/'.$data->id.'/edit" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a onclick="comfirmdelete('.$data->id.')" class="btn btn-xs btn-danger text-white"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    public function create()
    {
        return view('admin.ekstrakulikuler.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEkstrakulikuler $request)
    {
        
        $ekstrakulikuler = new Ekstrakulikuler;
        $ekstrakulikuler->nama = $request->nama;
        $ekstrakulikuler->save();

        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Ekstrakulikuler  $ekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function show(Ekstrakulikuler $ekstrakulikuler)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Ekstrakulikuler  $ekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function edit(Ekstrakulikuler $ekstrakulikuler)
    {

        $data['data']=$ekstrakulikuler;
        $data['url']=$url = route('ekstrakulikuler.index');;
        return view('admin.ekstrakulikuler.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Ekstrakulikuler  $ekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEkstrakulikuler $request, Ekstrakulikuler $ekstrakulikuler)
    {
        
        $ekstrakulikuler = Ekstrakulikuler::find($ekstrakulikuler->id);

        $ekstrakulikuler->nama = $request->nama;

        $ekstrakulikuler->save();
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Ekstrakulikuler  $ekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ekstrakulikuler $ekstrakulikuler)
    {
        Ekstrakulikuler::destroy($ekstrakulikuler->id);
        return redirect()->back();
    }
}
