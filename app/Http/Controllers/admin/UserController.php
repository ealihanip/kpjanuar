<?php

namespace App\Http\Controllers\admin;

use App\User;
use App\Roleuser;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata()
    {
        $data = Roleuser::with('user')->with('role')->where('role_id',1)->get();


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="user/'.$data->user_id.'/edit" class="btn btn-xs btn-primary"><i ></i> Edit</a>
                <a onclick="comfirmdelete('.$data->user_id.')" class="btn btn-xs btn-danger text-white"><i ></i> Hapus</a>
                ';
            })
            ->make(true);
    }

    public function create()
    {

        $data['role'] = Role::all();
        return view('admin.user.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        
        $user = new User;
        $user->name=$request->username;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->save();
        $user_id=$user->id;
        
        //memberi role user
        $user->roles()->attach($request->role);

        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        
        
        $data['role'] = Role::all();
        $data['user']=Roleuser::where('user_id',$user->id)->with('user')->with('role')->get();
        
        
        
        
        
       
        
        return view('admin.user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, User $user)
    {
        
        $user = User::find($user->id);

        $user->name=$request->username;
        
        //jika ubah password
        if($user->email!=$request->email){

            $request->validate([
                'email' => 'required|string|email|max:255|unique:users'
            ]);
            
            $user->email=$request->email;
        }

        if(!empty($request->password)){

            $request->validate([
                'password' => 'required|string|min:6|confirmed'
            ]);
            $user->password=Hash::make($request->password);
            
        }

        $user->save();


        $destroy=Roleuser::where('user_id',$user->id)->delete();
        $user->roles()->attach($request->role);
        
        $message = [
            'alert' => 'success',
            'title' => 'Data Berhasil Di Update'
        ];
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        User::destroy($user->id);
        $destroy=Roleuser::where('user_id',$user->id)->delete();
        return redirect()->back();
    }
}
