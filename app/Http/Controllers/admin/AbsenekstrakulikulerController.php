<?php

namespace App\Http\Controllers\admin;

use App\Models\Absenekstrakulikuler;
use App\Models\Mengajarekstrakulikuler;

use App\Models\Guru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use DB;
use PDF;

use Carbon\Carbon;



class AbsenekstrakulikulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (!empty($request->tahun)) {

            $bulan = $request->bulan;
            $tahun = $request->tahun;

            // $guru = Guru::with(['absenekstrakulikuler'=>function ($query) use($bulan,$tahun) {
            //     $query->whereYear('absenekstrakulikuler.created_at', '=', $tahun)->whereMonth('absenekstrakulikuler.created_at', '=',  $bulan);
            // }])->get();

            $jumHari = cal_days_in_month(CAL_GREGORIAN, $request->bulan, $request->tahun);
            
            
            $mengajarmapel=Mengajarekstrakulikuler::with('guru')->with('ekstrakulikuler')->with('absenekstrakulikuler')->get();
            
            $data_absen=array();
            
            foreach($mengajarmapel as $value){

                $absen=array();
                
                for($i=1;$i<=$jumHari;$i++){
                
                    
                    //misalkan 0 dan 1
                    $absen_at[$i]=0;
                    
                    foreach($value->absenekstrakulikuler as $absenekstrakulikuler){


                        
                        $tanggal=date('d', strtotime($absenekstrakulikuler->created_at));
                        $bulan=date('m', strtotime($absenekstrakulikuler->created_at));
                        $tahun=date('Y', strtotime($absenekstrakulikuler->created_at));

                        if($tanggal==$i && $bulan==$request->bulan && $tahun==$request->tahun){
                             //misalkan 0 dan 1
                             $absen_at[$i]++;
                        }

                    }

                    if($absen_at[$i]==0){
                        
                        $absen_at[$i]="";
                    }

                    $array=array(

                        'absen_at'=>$absen_at

                    );
                    $absen=$absen_at;
                    

                }

                $array=array(

                    'id'=>$value->id,
                    'nama_guru'=>$value->guru->nama,
                    'mata_pelajaran'=>$value->ekstrakulikuler->nama,
                    "absen"=>$absen,

                );

                array_push($data_absen,$array);
                
                

            }

           

            $data['bulan'] = $bulan;
            $data['tahun'] = $tahun;
            $data['absen'] = $data_absen;
            $data['jumlah_hari'] = $jumHari;

            $data['start'] = true;

        } else {

            $data = array();
            $data['start'] = false;
        }

        if ($request->cetak == true) {

            $this->cetak($data);
        }

        return view('admin.absenekstrakulikuler.index', $data);

    }


    public function getdataguru()
    {
        $data = Guru::all();


        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '
                <a href="guru/'.$data->id.'/show" class="btn btn-xs btn-primary"><i ></i> Lihat Absen</a>
                ';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        

        if(!empty($request->tahun)&&!empty($request->bulan)){

            $bulan=$request->bulan;
            $tahun=$request->tahun;
            $jumlahtanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
            $data['jumlah_tanggal']=$jumlahtanggal;

          
            $guru = Guru::with('absenekstrakulikuler')->find($request->dataabsenekstrakulikuler);


            $absen=array();
            for($i=1;$i<=$data['jumlah_tanggal'];$i++){

                $tanggal=date('Y-m-d',strtotime($tahun.'-'.$bulan.'-'.$i));
               
                $jumlah=0;
                foreach($guru->absenekstrakulikuler as $dataabsen){

                    $tanggalabsen = strtotime($dataabsen->created_at);
                    $tanggalabsen =date('Y-m-d',$tanggalabsen);
                    
                    if($tanggalabsen==$tanggal){

                        $jumlah++;
                        
                    }
                }

                $array=array(
                    'tanggal'=>$i,
                    'jumlah'=>$jumlah
                );

                array_push($absen,$array);
               

            }
            
            $data['guru']=array(

                'guru_id'=>$guru->id,
                'nama'=>$guru->nama,
                'bulan'=>$bulan,
                'tahun'=>$tahun,
                'absen'=>$absen,

            );


            $data['bulan']=$bulan;
            $data['tahun']=$tahun;

            if($request->cetak==true){

                $this->cetak($data['guru']);

            }
            
            return view('admin.absenekstrakulikuler.show',$data);

        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function edit(Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }

    function cetak($data) {

        $data['data']=$data;
        $pdf = PDF::loadView('admin/cetak/absenekstrakulikuler',$data,[], [
            'format' => 'A4-L'
        ]);
        return $pdf->download('document.pdf');
    }
}
