<?php

namespace App\Http\Controllers\guru;

use App\Models\Absenmapel;
use App\Models\Guru;
use App\Models\Mengajarmatapelajaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AbsenmapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guru_id=\Auth::user()->guru->id;
        $mengajarmatapelajaran = Mengajarmatapelajaran::where('guru_id',$guru_id)->get();
        $data['mengajarmatapelajaran']=$mengajarmatapelajaran;
        return view('guru.absenmatapelajaran.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //simpan ke guru
        $absenmatapelajaran = new Absenmapel;
        $absenmatapelajaran->mengajarmatapelajaran_id = $request->mengajarmatapelajaran;
        $absenmatapelajaran->save();

        

        $message = [
            'status'=> 'success',
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];

        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Absenmapel  $absenmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(Absenmapel $absenmatapelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Absenmapel  $absenmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Absenmapel $absenmatapelajaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Absenmapel  $absenmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absenmapel $absenmatapelajaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Absenmapel  $absenmatapelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absenmapel $absenmatapelajaran)
    {
        //
    }
}
