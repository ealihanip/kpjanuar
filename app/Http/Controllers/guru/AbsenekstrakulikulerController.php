<?php

namespace App\Http\Controllers\guru;

use App\Models\Absenekstrakulikuler;
use App\Models\Guru;
use App\Models\Mengajarekstrakulikuler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AbsenekstrakulikulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guru_id=\Auth::user()->guru->id;
        $mengajarekstrakulikuler = Mengajarekstrakulikuler::where('guru_id',$guru_id)->get();
        $data['mengajarekstrakulikuler']=$mengajarekstrakulikuler;
        return view('guru.absenekstrakulikuler.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //simpan ke guru
        $absenekstrakulikuler = new Absenekstrakulikuler;
        $absenekstrakulikuler->mengajarekstrakulikuler_id = $request->mengajarekstrakulikuler;
        $absenekstrakulikuler->save();

        

        $message = [
            'status'=> 'success',
            'alert' => 'success',
            'title' => 'Data Berhasil Di Simpan'
        ];

        return redirect()->back()->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function show(Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function edit(Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Absenekstrakulikuler  $absenekstrakulikuler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absenekstrakulikuler $absenekstrakulikuler)
    {
        //
    }
}
