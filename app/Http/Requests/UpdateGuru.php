<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGuru extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'nama' => 'required|min:3|max:30',
        ];
    }

    public function messages()
    {
        return [
            
            'nama.required' => 'Nama Guru Harus Di Isi',
            'nama.min' => 'Nama Guru Harus 3 huruf'
        ];
    }
}
