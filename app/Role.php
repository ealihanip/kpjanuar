<?php
namespace App;
use Zizaco\Entrust\EntrustRole;
class Role extends EntrustRole
{
    public function roleuser()
    {
        return $this->hasMany('App\Roleuser');
    }
}