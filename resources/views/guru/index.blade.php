@extends('guru.layouts.default')

@section('title', 'Ekstra Kulikuler')

@section('content')
    <br>
    <div class='container'>
        <div class='row'>
            <div class='col'>
            <div class="small-box bg-primary">
                <div class="inner">
                <h3>Absensi Mata Pelajaran</h3>

                <p>Isi Absen Mata Pelajaran Untuk Guru</p>
                </div>
                <div class="icon">
                <i class="ion ion-bag"></i>
                </div>
                <a href="{{ route('absenmatapelajaran.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
            
        </div>
        <hr>
        <div class='row'>
            <div class='col'>
            <div class="small-box bg-warning">
                <div class="inner">
                <h3>Absensi Ekstra Kulikuler</h3>

                <p>Isi Absen Ekstra Kuliler Untuk Guru</p>
                </div>
                <div class="icon">
                <i class="ion ion-bag"></i>
                </div>
                <a href="{{ route('absenekstrakulikuler.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            </div>
            
            
        </div>
    
    </div>



    
@endsection