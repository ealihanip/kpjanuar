@include('guru.includes.header')
@include('guru.includes.navbar')


<div class='container-fluid'>
  @yield('content')
</div>

@include('guru.includes.footer')