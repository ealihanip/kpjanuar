@extends('guru.layouts.default')

@section('title', 'Guru')

@section('content')
    <br>
    <br>
    <div class='container'>

        <div class='card'>
            <div class='card-body'>

                <div class='row'>

                    <div class='col-lg-12'>
                        <p class='h3'>Absen Mata Pelajaran</p>
                    </div>

                    <div class='col-lg-12'>
                        Tanggal : {{date('d-m-Y')}}
                        <hr>
                    </div> 

                    
                    <br>

                    <div class='col-lg-12'>
                        <div class='row justify-content-md-center'>
                            <div class='col-lg-4'>

                                <div class='card bg-light'>
                                    <div class='card-body'>
                                        <form action="{{ route('absenmatapelajaran.store') }}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            

                                            <!-- field -->
                                            <div class="form-group {{ $errors->has('mengajarmatapelajaran') ? 'has-error' : '' }}">
                                                <label for="mengajaramatapelajaran">Mata Pelajaran</label>
                                                <select class="form-control" name="mengajarmatapelajaran">
                                                    @foreach ($mengajarmatapelajaran as $data)
                                                        <option value="{{ $data->id }}" >{{ $data->matapelajaran->nama }}</option>
                                                    @endforeach
                                                </select>
                                                    
                                                @if ($errors->has('bulan'))
                                                    <span class="help-block text-danger">{{ $errors->first('mengajarmatapelajaran') }}</span>
                                                @endif
                                            </div>
                                            <!-- end field -->
                                            
                                            
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-dark w-100">Absen</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                                    
                    </div>
                    
                    
                    
                    
                
                
                </div>

            </div>
        </div>
        
        <br>
        

        @if (Session::has('message'))
        <div class="modal" tabindex="-1" role="dialog" id='alert'>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class='h3'>Absensi Mata Pelajaran Berhasil</p>
                </div>
                <div class="modal-footer">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>

        <script>


            

            $(document).ready( function () {
                
                $('#alert').modal('show')
                
            });
           


        </script>

        @endif

    </div>
    
    
@endsection