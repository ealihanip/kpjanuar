


@extends('admin.layouts.default')

@section('title', 'Tambah Data Mata Pelajaran')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('matapelajaran.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        

        <!-- field -->
        <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
            <label for="nama" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama">
            @if ($errors->has('nama'))
                <span class="help-block text-danger">{{ $errors->first('nama') }}</span>
            @endif
        </div>
        <!-- end field -->

        
        
        
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('matapelajaran.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

@endsection