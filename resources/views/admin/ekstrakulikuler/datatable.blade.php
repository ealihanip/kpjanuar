

<table class="table table-bordered" id="tabel-ekstrakulikuler">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Action</th>
            
        </tr>
    </thead>
</table>

<script>
$(document).ready( function () {
    $('#tabel-ekstrakulikuler').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('api/admin/ekstrakulikuler/getdata') }}',
        columns: [
            {data:'id'},
            {data:'nama'},
            {data:'action', orderable: false, searchable: false}
        ]
    });
});

</script>

