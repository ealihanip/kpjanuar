@extends('admin.layouts.default')

@section('title', 'Absen Mata Pelajaran')

@section('content')
    
    <div class='row'>
        <div class='col-lg-12'>
            
        </div>
        
        
    </div>
    <br>
    @if($start==false)
    <div class='row'>
        <div class='col-lg-12'>
            @include('admin.absenmatapelajaran.searchform')
        </div>
        
    </div>
    <hr>

    
            
           
    @else

    <div class='row'>
        <div class='col'>
            <a class="btn btn-dark" href="{{ route('dataabsenmatapelajaran.index') }}" role="button">Kembali</a>
            <a class='btn btn-dark text-white' href="{{ url('admin/dataabsenmatapelajaran/?bulan='.$bulan.'&tahun='.$tahun.'&cetak=true')}}">Cetak</a>
        
        </div>

        
        
        
    </div>
    <br>
    <br>

    
    <div class='row'>
        <div class='col-lg-12'>
            

            @include('admin.absenmatapelajaran.dataabsen')
            
           

        </div>
    @endif

        
    </div>
    <br>


    
    
@endsection