

<table class="table table-bordered" id="tabel-guru">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Action</th>
            
        </tr>
    </thead>
</table>

<script>
$(document).ready( function () {
    $('#tabel-guru').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('api/admin/absenmatapelajaran/getdataguru') }}',
        columns: [
            {data:'id'},
            {data:'nama'},
            {data:'action', orderable: false, searchable: false}
        ]
    });
});

</script>

