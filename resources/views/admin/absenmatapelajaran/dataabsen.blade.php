




<table class='table table-responsive table-bordered'>
    <thead>
            
                    <tr>
                        <th width="200px" rowspan="2">
                            Guru
                        </th>
            
                        <th width="200px" rowspan="2">
                            Mata Pelajaran
                        </th>
            
            
                        <th colspan="{{$jumlah_hari}}">
                            Tanggal
                        </th>
                        
            
                        <th rowspan='2'>
                            Total
                        </th>
                    </tr>
                    <tr>
                        
        
            
                        @for ($i = 1; $i <= $jumlah_hari; $i++)
                            <th width="40px">
                                {{$i}}
                            </th>
                        @endfor
            
                       
                    </tr>
                </thead>
            
                <tbody>
            
                    @foreach($absen as $absen)
            
                    <tr>
                        <td>
                            {{$absen['nama_guru']}}
                        </td>
            
            
                        <td>
                            {{$absen['mata_pelajaran']}}
                        </td>
            
                        
                        @php

                            $total=0;
                        @endphp

                        

                        @for ($i = 1; $i <= $jumlah_hari; $i++)
                            <td align="center">
                                {{$absen['absen'][$i]}}
                            </td>

                            @php
                                if(!empty($absen['absen'][$i])){

                                    $total=$total+$absen['absen'][$i];

                                }
                                
                            @endphp
                        @endfor
                        
                        <td align="center">
                            {{$total}}
                        </td>
                        
            
            
                    <tr>
            
            
                    @endforeach
                
                </tbody>
    
   
</table>