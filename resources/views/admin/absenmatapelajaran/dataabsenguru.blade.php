


<p class='h3'>Nama Guru : {{$guru['nama']}}</p>

<table class='table table-responsive table-bordered'>
    <thead>
        <tr>
            <th colspan='@php echo count($guru["absen"]) @endphp'>
                Tanggal
            </th>
                
            <th rowspan='2'>
                total
            </th>
        </tr>

        <tr>

        
        @foreach($guru['absen'] as $dataabsen)
            <th>
                {{$dataabsen['tanggal']}}
            </th>
        @endforeach
            
        </tr>
    </thead>

    <tbody>

    
        <tr>
            @php $total=0; @endphp
            @foreach($guru['absen'] as $dataabsen)
                <td>
                    {{$dataabsen['jumlah']}}
                </td>
                @php $total +=$dataabsen['jumlah']; @endphp
            @endforeach
                <td>
                    {{$total}}
                </td>
        </tr>
    </tbody>
    
   
</table>