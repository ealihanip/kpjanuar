


@extends('admin.layouts.default')

@section('title', 'Tambah Data Guru')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('guru.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        

        <!-- field -->
        <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
            <label for="nama" class="control-label">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama">
            @if ($errors->has('nama'))
                <span class="help-block text-danger">{{ $errors->first('nama') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" name="email" placeholder="email">
            @if ($errors->has('email'))
                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <!-- end field -->
        

        <div class="form-group">
            <label for="password" class="control-label">{{ __('Password') }}</label>

            
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block text-danger" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            
        </div>

        <div class="form-group">
            <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>

            
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            
        </div>

        
        
        
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('guru.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

@endsection