<html>

    <head>
        <style>
            table {
                border-collapse: collapse;
            }

            table, th, td {
                border: 1px solid black;
            }
        </style>
    </head>

    <body>
        <H1 align='center'>Data Absensi Ekstra Kulikuler</h1>
        <hr>
        <br>

            
        <p>Periode :
        
        @if($data['bulan']==1)
            
            Januari

        @elseif($data['bulan']==2)
            
            Februari
        
        @elseif($data['bulan']==3)
        
            Maret
        @elseif($data['bulan']==4)
            
            april
        @elseif($data['bulan']==5)
            
            Mei
        @elseif($data['bulan']==6)
            
            Juni
        @elseif($data['bulan']==7)
            
            Juli
        @elseif($data['bulan']==8)
            
            Agustus 
        @elseif($data['bulan']==9)
            
            September
        @elseif($data['bulan']==10)
            
            Oktober
        @elseif($data['bulan']==11)
            
            Nopemeber
        @elseif($data['bulan']==12)
            
            Desember
        

        @endif
        
        
        {{$data['tahun']}}<p>


       
            
        <hr>
        
        <table  style='width:100%; border:1px solid black;'>
                <thead>
            
                    <tr>
                        <th width="200px" rowspan="2">
                            Guru
                        </th>
            
                        <th width="200px" rowspan="2">
                            Mata Pelajaran
                        </th>
            
            
                        <th colspan="{{$jumlah_hari}}">
                            Tanggal
                        </th>
                        
            
                        <th rowspan='2'>
                            Total
                        </th>
                    </tr>
                    <tr>
                        
        
            
                        @for ($i = 1; $i <= $jumlah_hari; $i++)
                            <th width="40px">
                                {{$i}}
                            </th>
                        @endfor
            
                       
                    </tr>
                </thead>
            
                <tbody>
            
                    @foreach($absen as $absen)
            
                    <tr>
                        <td>
                            {{$absen['nama_guru']}}
                        </td>
            
            
                        <td>
                            {{$absen['mata_pelajaran']}}
                        </td>
            
                        
                        @php

                            $total=0;
                        @endphp

                        

                        @for ($i = 1; $i <= $jumlah_hari; $i++)
                            <td align="center">
                                {{$absen['absen'][$i]}}
                            </td>

                            @php
                                if($absen['absen'][$i]=="x"){

                                    $total++;

                                }
                                
                            @endphp
                        @endfor
                        
                        <td align="center">
                            {{$total}}
                        </td>
                        
            
            
                    <tr>
            
            
                    @endforeach
                
                </tbody>
                
                
            </table>


    </body>


</html>