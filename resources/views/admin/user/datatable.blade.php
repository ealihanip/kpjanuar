

<table class="table table-bordered" id="tabel-user">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama User</th>
            <th>Email</th>
            <th>Level</th>
            <th>Action</th>
            
        </tr>
    </thead>
</table>

<script>
$(document).ready( function () {
    $('#tabel-user').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('api/user/getdata') }}',
        columns: [
            {data:'user_id'},
            {data:'user.name'},
            {data:'user.email'},
            {data:'role.display_name'},
            {data:'action', orderable: false, searchable: false}
        ]
    });
});

</script>

