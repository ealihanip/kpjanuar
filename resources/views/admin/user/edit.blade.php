

@extends('admin.layouts.default')

@section('title', 'Edit Kriteria Dan Bobot')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    @foreach($user as $user)
    <form action='{{ route("user.update",["id" => $user->user_id]) }}' method="POST">
        @method('PUT')
        {{csrf_field()}}
        
        
        <!-- field -->
        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
            <label for="username" class="control-label">Username</label>
            <input type="text" class="form-control" name="username" placeholder="username" value='{{$user->user->name}}'>
            @if ($errors->has('username'))
                <span class="help-block text-danger">{{ $errors->first('username') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" name="email" placeholder="email" value='{{$user->user->email}}'>
            @if ($errors->has('email'))
                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
            <label for="role" class="control-label">Level</label>
            <select class="form-control" name="role">
                @foreach ($role as $value)
                    <option value='{{ $value->id }}'
                    
                    @if ($value->id==$user->role_id)
                        selected
                    @endif    
                    
                    
                    >{{ $value->display_name }}</option>
                @endforeach
            </select>
            @if ($errors->has('role'))
                <span class="help-block text-danger">{{ $errors->first('role') }}</span>
            @endif
        </div>
        <!-- end field -->
        
        <div class="form-group">
            <label for="password" class="control-label">{{ __('Password') }}</label>

            
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

            @if ($errors->has('password'))
                <span class="help-block text-danger" role="alert">
                   {{ $errors->first('password') }}
                </span>
            @endif
            
        </div>

        <div class="form-group">
            <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>

            
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
            
        </div>

        
        
        
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ route('user.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>
   
    @endforeach


    

@endsection

