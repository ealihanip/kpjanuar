<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      
        <span class="brand-text font-weight-light">Absensi Guru</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('public/assets/image/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      @php
        $auth=Auth::user()->roleuser;
        

        foreach($auth as $auth){

          $role=$auth->role_id;
          
        }
      @endphp

       <!-- Sidebar Menu -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
          
          <li class="nav-item">
            <a href="{{ url('admin/guru') }}" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Guru
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/matapelajaran') }}" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Mata Pelajaran
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/ekstrakulikuler') }}" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Ekstra Kulikuler
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/mengajarmatapelajaran') }}" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Mengajar Mata Pelajaran
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/mengajarekstrakulikuler') }}" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Mengajar Ekstra Kulikuler
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/dataabsenmatapelajaran') }}" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Absen Mata Pelajaran
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/dataabsenekstrakulikuler') }}" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Absen Ekstra Kulikuler
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('admin/user') }}" class="nav-link">
              <i class="nav-icon fa fa-key"></i>
              <p>
                User
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="nav-icon fa fa-sign-out"></i>
                {{ __('Logout') }}
            </a>
            
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
          
          

          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  