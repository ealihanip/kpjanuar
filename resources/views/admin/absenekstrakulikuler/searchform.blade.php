<form>
            <div class="row">  
                <div class="col">
                <select class="form-control" name="bulan">
                    <option value='1'

                        @if (isset($bulan))
                            @if ($bulan==1)
                                selected
                            @endif
                            
                        @endif
                    
                    >Januari
                    </option>
                    <option value='2'

                        @if (isset($bulan))
                            @if ($bulan==2)
                                selected
                            @endif
                            
                        @endif
                    
                    >Februari
                    </option>
                    <option value='3'

                        @if (isset($bulan))
                            @if ($bulan==3)
                                selected
                            @endif
                            
                        @endif
                    
                    >Maret
                    </option>

                    <option value='4'

                        @if (isset($bulan))
                            @if ($bulan==4)
                                selected
                            @endif
                            
                        @endif
                    
                    >April
                    </option>

                    <option value='5'

                        @if (isset($bulan))
                            @if ($bulan==5)
                                selected
                            @endif
                            
                        @endif
                    
                    >Mei
                    </option>

                    <option value='6'

                        @if (isset($bulan))
                            @if ($bulan==6)
                                selected
                            @endif
                            
                        @endif
                    
                    >Juni
                    </option>

                    <option value='7'

                        @if (isset($bulan))
                            @if ($bulan==7)
                                selected
                            @endif
                            
                        @endif
                    
                    >Juli
                    </option>

                    <option value='8'

                        @if (isset($bulan))
                            @if ($bulan==8)
                                selected
                            @endif
                            
                        @endif
                    
                    >Agustus
                    </option>

                    <option value='9'

                        @if (isset($bulan))
                            @if ($bulan==9)
                                selected
                            @endif
                            
                        @endif
                    
                    >September
                    </option>

                    <option value='10'

                        @if (isset($bulan))
                            @if ($bulan==10)
                                selected
                            @endif
                            
                        @endif
                    
                    >Oktober
                    </option>

                    <option value='11'

                        @if (isset($bulan))
                            @if ($bulan==11)
                                selected
                            @endif
                            
                        @endif
                    
                    >November
                    </option>

                    <option value='12'

                        @if (isset($bulan))
                            @if ($bulan==12)
                                selected
                            @endif
                            
                        @endif
                    
                    >Desember
                    </option>
                    

                </select>
                </div>
               
                <div class="col">
                <select class="form-control" name="tahun">
                    @for ($i = 2018; $i <= 2030; $i++)
                    
                    <option value='{{$i}}'

                        @if (isset($bulan))
                            @if ($bulan==$i)
                                selected
                            @endif
                            
                        @endif
                    
                    >{{$i}}
                    </option>
                    @endfor
                </select>
                </div>
            </div>
            <br>
            <div class="row">

                <div class="col-lg-4">
                    <input type="submit" class="btn btn-dark form-control" value='Cari'>
                </div>
                
            </div>
        </form>