@extends('admin.layouts.default')

@section('title', 'Absen Ekstra Kulikuler')

@section('content')
    
    <div class='row'>
        <div class='col-lg-12'>
            
        </div>
        
        
    </div>
    <br>
    

    <div class='row'>
        <div class='col'>
            <a class="btn btn-dark" href="{{ route('dataabsenekstrakulikuler.index') }}" role="button">Kembali</a>
            <a class='btn btn-dark text-white' href="{{ url('admin/dataabsenekstrakulikuler/'.$guru['guru_id'].'?bulan='.$bulan.'&tahun='.$tahun.'&cetak=true')}}">Cetak</a>
        </div>
        
        
    </div>
    <br>
    <br>

    
    <div class='row'>
        <div class='col-lg-12'>
            

            @include('admin.absenekstrakulikuler.dataabsenguru')

           

        </div>
    

        
    </div>
    <br>


    <div class="modal" tabindex="-1" role="dialog" id='modalcomfirmdelete'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Komfirmasi Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method='post' id="formhapus">
                    @method('DELETE')
                    {{csrf_field()}}
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Ya Hapus</button>
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <script>


        function comfirmdelete(id){

            $(document).ready( function () {
                
                $('#modalcomfirmdelete').modal('show')
               
            });
        }


    </script>
    
@endsection