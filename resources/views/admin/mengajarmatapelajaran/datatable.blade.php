

<table class="table table-bordered" id="tabel-matapelajaran">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Guru</th>
            <th>Nama Matapelajaran</th>
            <th>Action</th>
            
        </tr>
    </thead>
</table>

<script>
$(document).ready( function () {
    $('#tabel-matapelajaran').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('api/admin/mengajarmatapelajaran/getdata') }}',
        columns: [
            {data:'id'},
            {data:'guru.nama'},
            {data:'matapelajaran.nama'},
            {data:'action', orderable: false, searchable: false}
        ]
    });
});

</script>

