

@extends('admin.layouts.default')

@section('title', 'Edit Mengajar Mata Pelajaran')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action='{{ route("mengajarmatapelajaran.update",["id" => $data->id]) }}' method="POST">
        @method('PUT')
        {{csrf_field()}}
        
        

        <!-- field -->
        <div class="form-group {{ $errors->has('guru') ? 'has-error' : '' }}">
            <label for="guru">Guru</label>
            <select class="form-control" name="guru">
                @foreach ($guru as $value)
                    <option value="{{ $value->id }}" @if($data->guru_id==$value->id) selected @endif >{{ $value->nama }} </option>
                @endforeach
            </select>
				
            @if ($errors->has('guru'))
                <span class="help-block text-danger">{{ $errors->first('guru') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('matapelajaran') ? 'has-error' : '' }}">
            <label for="matapelajaran">Mata Pelajaran</label>
            <select class="form-control" name="matapelajaran">
                @foreach ($matapelajaran as $value)
                    <option value="{{ $value->id }}" @if($data->matapelajaran_id==$value->id) selected @endif >{{ $value->nama }}</option>
                @endforeach
            </select>
				
            @if ($errors->has('guru'))
                <span class="help-block text-danger">{{ $errors->first('matapelajaran') }}</span>
            @endif
        </div>
        <!-- end field -->
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('mengajarmatapelajaran.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>



    

@endsection

