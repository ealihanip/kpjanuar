@extends('admin.layouts.default')

@section('title', 'Mengajar Ekstra Kulikuler')

@section('content')
    
    <div class='row'>
        <div class='col'>
            <a class="btn btn-primary" href="{{ route('mengajarekstrakulikuler.create') }}" role="button">Tambah Data</a>
        </div>
        
        
    </div>
    <br>
    <div class='row'>
        <div class='col'>
            @include('admin.mengajarekstrakulikuler.datatable')
        </div>
        
    </div>


    <div class="modal" tabindex="-1" role="dialog" id='modalcomfirmdelete'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Komfirmasi Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method='post' id="formhapus">
                    @method('DELETE')
                    {{csrf_field()}}
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Ya Hapus</button>
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>

    <script>


        function comfirmdelete(id){

            $(document).ready( function () {
                url="{{ url('admin/mengajarekstrakulikuler/') }}"+'/'+id;
                $('#modalcomfirmdelete').modal('show')
                $("#formhapus").attr("action", url);
            });
        }


    </script>
    
@endsection