


@extends('admin.layouts.default')

@section('title', 'Tambah Data Mengajar Ekstra Kulikuler')

@section('content')
   
    
    {{-- part alert --}}
    @if (Session::has('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('message.alert') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message.title') }}</strong>
                    
                </div>
            </div>
        </div>
    @endif
    
    <form action="{{ route('mengajarekstrakulikuler.store') }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        

        <!-- field -->
        <div class="form-group {{ $errors->has('guru') ? 'has-error' : '' }}">
            <label for="guru">Guru</label>
            <select class="form-control" name="guru">
                @foreach ($guru as $data)
                    <option value="{{ $data->id }}" >{{ $data->nama }}</option>
                @endforeach
            </select>
				
            @if ($errors->has('guru'))
                <span class="help-block text-danger">{{ $errors->first('guru') }}</span>
            @endif
        </div>
        <!-- end field -->

        <!-- field -->
        <div class="form-group {{ $errors->has('matapelajaran') ? 'has-error' : '' }}">
            <label for="ekstrakulikuler">Ekstra Kulikuler</label>
            <select class="form-control" name="ekstrakulikuler">
                @foreach ($ekstrakulikuler as $data)
                    <option value="{{ $data->id }}" >{{ $data->nama }}</option>
                @endforeach
            </select>
				
            @if ($errors->has('guru'))
                <span class="help-block text-danger">{{ $errors->first('matapelajaran') }}</span>
            @endif
        </div>
        <!-- end field -->

       
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="{{ route('mengajarekstrakulikuler.index') }}" class="btn btn-default">Kembali</a>
        </div>
    </form>

@endsection