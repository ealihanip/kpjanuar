

<table class="table table-bordered" id="tabel">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Guru</th>
            <th>Nama Matapelajaran</th>
            <th>Action</th>
            
        </tr>
    </thead>
</table>

<script>
$(document).ready( function () {
    $('#tabel').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('api/admin/mengajarekstrakulikuler/getdata') }}',
        columns: [
            {data:'id'},
            {data:'guru.nama'},
            {data:'ekstrakulikuler.nama'},
            {data:'action', orderable: false, searchable: false}
        ]
    });
});

</script>

