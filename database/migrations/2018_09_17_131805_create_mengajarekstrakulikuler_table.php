<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMengajarekstrakulikulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mengajarekstrakulikuler', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ekstrakulikuler_id');
            $table->unsignedInteger('guru_id');
            $table->timestamps();
            $table->foreign('ekstrakulikuler_id')->references('id')->on('ekstrakulikuler')->onDelete('cascade');;
            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade');; 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mengajarekstrakulikuler');
    }
}
