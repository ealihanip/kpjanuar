<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMengajarmatapelajaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mengajarmatapelajaran', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('matapelajaran_id');
            $table->unsignedInteger('guru_id');
            $table->timestamps();
            $table->foreign('matapelajaran_id')->references('id')->on('matapelajaran')->onDelete('cascade');;
            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade');; 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mengajarmatapelajaran');
    }
}
