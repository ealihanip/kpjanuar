<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenekstrakulikulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absenekstrakulikuler', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mengajarekstrakulikuler_id');
            $table->foreign('mengajarekstrakulikuler_id')->references('id')->on('mengajarekstrakulikuler')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absenekstrakulikuler');
    }
}
