<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenmatapelajaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absenmatapelajaran', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mengajarmatapelajaran_id');
            $table->foreign('mengajarmatapelajaran_id')->references('id')->on('mengajarmatapelajaran')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absenmatapelajaran');
    }
}
